import React,{useState} from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import RadioForm from 'react-native-simple-radio-button';

export default function App() {
  const [weight,setWeight] = useState('');
  const [bottles,setBottles] = useState(1);
  const [time,setTime] = useState(1);
  const [gender,setGender] = useState('male');
  const [promilles,setPromilles] = useState(0);
  

  function calculate(){
    if (!weight.trim()){
      alert('Please enter your weight');
      return;
    }

    let litres = bottles * 0.33;
    let grams = litres * 8 * 4.5;
    let burning = weight / 10;
    let gramsLeft = grams - burning * time;
    let result = 0;
    if (gender === 'male'){
      result = gramsLeft / (weight * 0.7);
      if (result < 0){
        result = 0;
      }
    }else{
      result = gramsLeft / (weight * 0.6);
      if (result < 0){
        result = 0;
      }
    }
    setPromilles(result)
  }

  return (
    <View style={styles.container}>
      <View style={styles.field}>
        <Text>Weight</Text>
        <TextInput 
          style={styles.input}
          keyboardType="numeric" 
          placeholder="Enter weight"
          value={weight}
          onChangeText={text => setWeight(text)}
          >
        </TextInput>
      </View>
      {/*<View>*/}
        <Text style={styles.dropdowntext}>Bottles</Text>
        <DropDownPicker style={styles.dropdown} items={[
          {label: '1 bottle', value: 1},
          {label: '2 bottles', value: 2},
          {label: '3 bottles', value: 3},
          {label: '4 bottles', value: 4},
          {label: '5 bottles', value: 5},
          {label: '6 bottles', value: 6},
          {label: '7 bottles', value: 7}
        ]}
          containerStyle={{height: 40}}
          defaultValue={bottles}
          onChangeItem={item => setBottles(item.value)}
          labelStyle={{color:'#000'}}
        >
        </DropDownPicker>
      {/*</View>*/}
      {/*<View>*/}
        <Text style={styles.dropdowntext}>Time</Text>
        <DropDownPicker style={styles.dropdown} items={[
          {label: '1 hour', value: 1},
          {label: '2 hours', value: 2},
          {label: '3 hours', value: 3},
          {label: '4 hours', value: 4},
          {label: '5 hours', value: 5},
          {label: '6 hours', value: 6}
        ]}
          containerStyle={{height: 40}}
          defaultValue={time}
          onChangeItem={item => setTime(item.value)}
          labelStyle={{color:'#000'}}
        >
        </DropDownPicker>
      {/*</View>*/}
      <View style={styles.field}>
        <Text>Gender</Text>
        <RadioForm style={styles.radio}
        radio_props={[
          { label: 'Male', value: 'male'},
          { label: 'Female', value: 'female'}
        ]}
          onPress={(value) => {setGender(value)}}
        >
        </RadioForm>
      </View>
      <View style={styles.field}>
        <Text>Promilles</Text>
        <Text>{promilles.toFixed(2)}</Text>
        <Button onPress={calculate}
        title="Calculate"></Button>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  input: {
    marginTop: 10,
  },
  dropdowntext: {
    marginLeft: 10,
  },
  dropdown:{
    marginLeft: 10,
  },
  field: {
    margin: 10,
  },
});
